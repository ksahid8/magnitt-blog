import SidebarCategories from "./sidebar-categories";
import SidebarTags from "./sidebar-tags";

const RightBar = () => {
  return (
    <div className="right-bar">
      <SidebarTags />
      <SidebarCategories />
    </div>
  );
};
export default RightBar;
