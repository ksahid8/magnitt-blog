import Link from "next/link";
import { Container } from "react-bootstrap";

const Header = () => {
  return (
    <header>
      <Container>
        <nav className="navbar">
          <div className="navbar-brand" href="#">
            <Link href="/">
              <a className="">
                <h4>My Blog</h4>
              </a>
            </Link>
          </div>
          <div className="nav-links">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <Link href="/">
                  <a className="nav-link button" href="#">
                    All Posts <span className="sr-only">(current)</span>
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </Container>
    </header>
  );
};

export default Header;
