import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import data from "./../../lib/categories.json";

const SidebarCategories = () => {
  const Categories = data.categories;

  const CategoriesList = Categories.map((key) => {
    return (
      <li className="d-flex"  key={key}>
        <a href="#">
          <FontAwesomeIcon icon={faAngleRight} />
          {key}
        </a>
      </li>
    );
  });

  return (
    <div className="sidebar-categories">
      <h5 className="rightbar-title">Categories</h5>
      <div className="sidebar-container borders">
        <ul className="sidebar-list">{CategoriesList}</ul>
      </div>
    </div>
  );
};
export default SidebarCategories;
