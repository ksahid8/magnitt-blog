import Meta from "./meta";
import Header from "./header";
import Footer from "./footer";
import { Container, Row, Col } from "react-bootstrap";
import RightBar from "./right-bar";
import PageWrapper from "./page-wrapper";

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <Meta />
      <Container className="main">
        <Row>
          <Col md="9">
            <PageWrapper>{children}</PageWrapper>
          </Col>
          <Col md="3">
            <RightBar />
          </Col>
        </Row>
      </Container>
      <Footer />
    </>
  );
};
export default Layout;
