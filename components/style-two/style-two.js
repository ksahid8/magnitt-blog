import Link from "next/link";
import { Col } from "react-bootstrap";
import moment from "moment";
import MomentConfig from "../../lib/moment-config";
import EllipsisText from "react-ellipsis-text";
import Tags from "../tags";
export default function StyleTwo({ title, coverImage, date, description, slug,tags }) {
  return (
    <Col md="6">
      <div className="style-two-card">
        <div className="grid-contain">
          <div className="image-container">
            <Link as={`/posts/${slug}`} href="/posts/[slug]">
              <img alt="" className="img-fluid" src={coverImage}></img>
            </Link>
          </div>
          <div className="text-container">
            <div className="blog-info">
              <Link as={`/posts/${slug}`} href="/posts/[slug]">
                <h3>
                  <EllipsisText text={title} length={55} />
                </h3>
              </Link>
              <p>
                {" "}
                <EllipsisText text={description} length={210} />
              </p>
              <div className="blog-time mb-2">
                {moment(date).format(MomentConfig.MOMENT_FORMAT_DATEANDTIME)}
              </div>
              <Tags tags={tags} />
            </div>
          </div>
        </div>
      </div>
    </Col>
  );
}
