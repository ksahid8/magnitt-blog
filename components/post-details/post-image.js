import Link from "next/link";

const PostImage = ({ title, src, slug }) => {
  const image = (
    <img src={src} alt={`Cover Image for ${title}`} className="img-fluid" />
  );
  return (
    <>
      {slug ? (
        <Link as={`/posts/${slug}`} href="/posts/[slug]">
          <a aria-label={title}>{image}</a>
        </Link>
      ) : (
        image
      )}
    </>
  );
};

export default PostImage;
