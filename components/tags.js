import Link from "next/link";
import { useEffect, useState } from "react";
const Tags = ({ tags }) => {
  const [Tags, setTags] = useState([]);
  const PostTags = Tags.map((tag) => {
    return <Link as={`/posts/tags/${tag}`} href="/posts/tags/[tag]" key={tag}><span>{tag}</span></Link>;
  });

  useEffect(() => {
    if (tags && tags.length) setTags(tags);
  }, [tags]);

  return (
    <div className="blog-tags">
      <label>Tags :</label>
      {PostTags}
    </div>
  );
};
export default Tags;
