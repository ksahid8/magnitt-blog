import { useRouter } from "next/router";
import MorePosts from "../../../components/more-posts";
import Layout from "../../../components/partials/layout";
import { useEffect, useState } from "react";
import Head from "next/head";
import { CMS_NAME } from "../../../lib/constants";
import { Row } from "react-bootstrap";
import data from "./../../../lib/data.json";

export default function Tag() {
  const router = useRouter();
  const { tag } = router.query;

  const [AllPosts, setAllPosts] = useState(data.data);

  const [PostsList, setPostsList] = useState([]);

  let abc = [];
  useEffect(() => {
    if (tag && AllPosts && AllPosts.length) {
      abc = AllPosts.filter((item) => {
        return item.tags.includes(tag);
      });
      setPostsList(abc);
    }
  }, [AllPosts, tag]);

  return (
    <>
      <Layout>
        <Head>
          <title>
            {CMS_NAME} | {tag}
          </title>
        </Head>
        <Row>{PostsList.length > 0 && <MorePosts posts={PostsList} />}</Row>
      </Layout>
    </>
  );
}
